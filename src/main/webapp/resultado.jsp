<%-- 
    Document   : resultado
    Created on : 21-09-2021, 12:05:49
    Author     : axeld
--%>

<%@page import="mvc.model.Model"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
        <% 

         Model model = (Model)request.getAttribute("model");

        %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Resultado:  </h1>
        <div>Capital: $<%=model.getCapital() %></div>
        <div>Interes anual: <%=Math.round(model.getIntAnual())%>%</div>
        <div>Cantidad de años: <%=model.getAnios() %></div>
        <div>Total interes Simple: $<%=Math.round(model.getInteresSimple()) %></div>

    </body>
</html>
