<%-- 
    Document   : index
    Created on : 14-09-2021, 14:38:21
    Author     : axeld
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Calculadora interes simple</h1>
        <form action="Controller"method="POST">
            <div class="form-group">
                <label for="capital">Capital: </label>
                <input type="capital" name="capital" class="form-control" id="capital">
            </div>
            <div class="form-group">
                <label for="intAnual">Interes anual: </label>
                <input type="intAnual" name="intAnual"class="form-control" id="intAnual">
            </div>
            <div class="form-group">
                <label for="anios">Años:  </label>
                <input type="anios" name="anios"class="form-control" id="anios">
            </div>
            
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </body>
</html>
