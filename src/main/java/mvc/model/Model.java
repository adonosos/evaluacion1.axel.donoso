/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.model;

/**
 *
 * @author axeld
 */
public class Model {
    private double interesSimple;
    private int capital;
    private double intAnual;
    private int anios;

    public Model(String capital, String intAnual, String anios) {
        this.capital = Integer.parseInt(capital);
        this.intAnual = Double.parseDouble(intAnual);
        this.anios = Integer.parseInt(anios);
    }

    

    public double getInteresSimple() {
        return (capital*(intAnual/100)*anios);
    }

    /**
     * @param interesSimple the interesSimple to set
     */
    public void setInteresSimple(double interesSimple) {
        this.interesSimple = interesSimple;
    }

    /**
     * @return the capital
     */
    public int getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(int capital) {
        this.capital = capital;
    }

    /**
     * @return the intAnual
     */
    public double getIntAnual() {
        return intAnual;
    }

    /**
     * @param intAnual the intAnual to set
     */
    public void setIntAnual(double intAnual) {
        this.intAnual = intAnual;
    }

    /**
     * @return the anios
     */
    public int getAnios() {
        return anios;
    }

    /**
     * @param anios the anios to set
     */
    public void setAnios(int anios) {
        this.anios = anios;
    }

    

}
